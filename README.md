<p align="right">
    <a href="https://badge.fury.io/rb/just-the-docs"><img src="https://badge.fury.io/rb/just-the-docs.svg" alt="Gem version"></a> <a href="https://github.com/pmarsceill/just-the-docs/actions?query=workflow%3A%22Master+branch+CI%22"><img src="https://github.com/pmarsceill/just-the-docs/workflows/Master%20branch%20CI/badge.svg" alt="Build status"></a>
</p>
<br><br>
<p align="center">
    <h1 align="center">Just the Docs</h1>
    <p align="center">A modern, highly customizable, and responsive Jekyll theme for documentation with built-in search.<br>Easily hosted on GitHub Pages with few dependencies.</p>
    <p align="center"><strong><a href="https://pmarsceill.github.io/just-the-docs/">See it in action!</a></strong></p>
    <br><br><br>
</p>

![jtd](https://user-images.githubusercontent.com/896475/47384541-89053c80-d6d5-11e8-98dc-dba16e192de9.gif)

# just-the-docs -  WIN GitLab Pages theme

A simple and customisable theme for buiding your GitLab pages site. 

## Usage

### 1. Fork this project

Fork this project by clicking the __*Fork*__ button at the top right corner of this page. Forking means that you now copied this entire project and all the files into your account.

### 2. Rename the project

In your version of the project, click on __*Settings*__ on the left (the cog icon) and rename the project (*Project name*) to something meaningful to you. In the __*Advanced*__ settings (bottom of the page), check that the path matches what you expect given your desired project name. 

### 3. Set the _config.yml url and update the "served at " description

In the file `_config.yml`, update line 18 so the baseurl matches the path of your project. The baseurl should start with `"/pages/"` and end without a trailing `/`. 

In the project settings, update the description to match the address of where your build page will be rendered. This is the combnined url+baseurl addresses from your `_config.yml`.

### 4. Turn on CI Runners

You need to enable a "runner" in order for GitLab to "build"/"serve" your page. Go to __*Settings*__ > __*CI/CD*__ > __*Runners*__. Tick `enable shared runners`.

### 5. Customize your website settings

Edit the `_config.yml` file to change any additional settings you want. To edit the file, click on it to view the file and then click on the pencil icon to edit it.  The settings in the file are self-explanatory and there are comments inside the file to help you understand what each setting does. Any line that begins with a hashtag (`#`) is a comment, and the other lines are actual settings.

**We suggest you start by making a small change (for example updating the title or author) then commit this change. Making the commit will trigger the CI, which will take a few moments to build your page.** You can monitor the progress of the CI in `CI/CD pipleines` (rocket icon on the left navigation bar).

### 6. Add your own content

To add pages to your site, you can either write a markdown file (`.md`) or you can write an HTML file. It's much easier to write markdown than HTML, so that's the recommended approach ([here's a great tutorial](https://markdowntutorial.com/) if you need to learn markdown in 5 minutes).

To see an example of a markdown file, click on any file that ends in `.md`, for example [`aboutme.md`](./aboutme.md). On that page you can see some nicely formatted text (there's a word in bold, a link, a few bullet points), and if you click on the pencil icon to edit the file, you'll see the markdown code that generated the pretty text. Very easy!

[View the documentation](https://pmarsceill.github.io/just-the-docs/) for additional usage usage information.

Take a look at the [Open WIN Community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/) to see an example of a customised version of this theme. Don't forget to look at the GitLab repository for that site (follow the "[view on GitLab link](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community)" to see how the customisation was acheived.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/pmarsceill/just-the-docs. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

### Submitting code changes:

- Open a [Pull Request](https://github.com/pmarsceill/just-the-docs/pulls)
- Ensure all CI tests pass
- Await code review
- Bump the version number in `just-the-docs.gemspec` and `package.json` according to [semantic versioning](https://semver.org/).

### Design and development principles of this theme:

1. As few dependencies as possible
2. No build script needed
3. First class mobile experience
4. Make the content shine

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is set up just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When the theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
